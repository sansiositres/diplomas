import pandas as pd

h = pd.read_csv('context_h.csv')
f = pd.read_csv('context_f.csv')
print(h[:5])
h = h[['email', 'name', 'surname', 'pdf']]
h = h.rename(columns = {'pdf': 'file'})
h['file'] = h['file'].apply(lambda x: str(x)+'.pdf')
h['league'] = 'Высшей'
print(h[:5])
h.to_csv('high_league_email.csv')

f = f[['email', 'name', 'surname', 'pdf']]
f = f.rename(columns = {'pdf': 'file'})
f['file'] = f['file'].apply(lambda x: '1_'+str(x)+'.pdf')
f['league'] = 'Первой'
print(f[:5])
f.to_csv('first_league_email.csv')
h.drop('Unnamed: 0', axis=1, inplace=True)
f.drop('Unnamed: 0', axis=1, inplace=True)
n = pd.DataFrame(data=[['sansiositres@gmail.com', 'Виолетта', 'Мишечкина', '1.pdf', 'Первой']],
                          columns=['email', 'name', 'surname', 'file', 'league'])
h = h.append(n, ignore_index=True)
f = f.append(n, ignore_index=True)
h.drop_duplicates(subset='email', keep='first', inplace=True)
f.drop_duplicates(subset='email', keep='first', inplace=True)
h.to_csv('context_h.csv', index=False, sep=',')
f.to_csv('context_f.csv', index=False, sep=',')
