from PyPDF2 import PdfFileReader, PdfFileWriter
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
import io
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.units import inch
import pandas as pd


# -*- coding: utf-8 -*-

high_league = pd.read_csv('final_results_april_och.csv')
high_league = high_league.sort_values('Фамилия')
output = PdfFileWriter()
for row in high_league.itertuples():
    input1 = PdfFileReader(open('diplom2019.pdf', 'rb'))
    # print(row)
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)
    pdfmetrics.registerFont(TTFont('FreeSans', 'FreeSans.ttf'))
    can.setFont('FreeSans', 48)
    can.drawCentredString(310, 490, 'СЕРТИФИКАТ')
    can.drawCentredString(310, 440, 'УЧАСТНИКА')
    can.setFont('FreeSans', 32)
    can.drawCentredString(310, 290, row.Имя + ' ' + row.Фамилия)
    can.setFont('FreeSans', 22)
    can.drawCentredString(310, 380, 'очного тура II Санкт-Петербургской')
    can.drawCentredString(310, 355, 'олимпиады по программированию 2019 года')
    can.drawCentredString(310, 325, 'среди ' + str(int(row.Класс)) + ' классов')
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    p1 = input1.getPage(0)
    p1.mergePage(new_pdf.getPage(0))
    output.addPage(p1)
    del can
    del p1
    del new_pdf

output.write(open('diploma'+'.pdf', 'wb'))
