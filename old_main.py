from PyPDF2 import PdfFileReader, PdfFileWriter
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
import io
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.units import inch
import pandas as pd


# -*- coding: utf-8 -*-

reg = pd.read_csv('data_feb.csv')
high_league = pd.read_csv('high_league.csv')
first_league = pd.read_csv('first_league.csv')
high_league = pd.merge(high_league, reg[['name', 'surname', 'email']], on=['name', 'surname'])
first_league = pd.merge(first_league, reg[['name', 'surname', 'email']], on=['name', 'surname'])
places = {1: 'I', 2: 'II', 3: 'III'}

for row in high_league.itertuples():
    input1 = PdfFileReader(open('diplom2019.pdf', 'rb'))
    print(row)
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)
    pdfmetrics.registerFont(TTFont('FreeSans', 'FreeSans.ttf'))
    can.setFont('FreeSans', 32)
    can.drawCentredString(310, 400, row.name + ' ' + row.surname)
    can.setFont('FreeSans', 22)
    string = places[row.place] + ' место в Высшей лиге'
    can.drawCentredString(310, 360, string)
    can.drawCentredString(310, 335, 'среди ' + str(row.grade) + ' классов')
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    output = PdfFileWriter()
    p1 = input1.getPage(0)
    p1.mergePage(new_pdf.getPage(0))
    output.addPage(p1)
    output.write(open(str(row[0])+'.pdf', 'wb'))
    del can
    del p1
    del output
    del new_pdf

high_league.to_csv('high_league_email.csv', index_label='pdf')

for row in first_league.itertuples():
    input1 = PdfFileReader(open('diplom_febcube_2019.pdf', 'rb'))
    print(row)
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=letter)
    pdfmetrics.registerFont(TTFont('FreeSans', 'FreeSans.ttf'))
    can.setFont('FreeSans', 32)
    can.drawCentredString(310, 400, row.name + ' ' + row.surname)
    can.setFont('FreeSans', 22)
    string = places[row.place] + ' место в Первой лиге'
    can.drawCentredString(310, 360, string)
    can.drawCentredString(310, 335, 'среди ' + str(row.grade) + ' классов')
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    output = PdfFileWriter()
    p1 = input1.getPage(0)
    p1.mergePage(new_pdf.getPage(0))
    output.addPage(p1)
    output.write(open('1_' + str(row[0])+'.pdf', 'wb'))
    del can
    del p1
    del output
    del new_pdf
first_league.to_csv('first_league_email.csv', index_label='pdf')